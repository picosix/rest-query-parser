const startsWith = require("lodash/startsWith");

module.exports = sort => {
  if (typeof sort !== "string" || !sort) return {};

  let sortFields = sort
    .split(",")
    .map(f => f.trim())
    .filter(f => f && f !== "-");

  return sortFields.reduce((sortProps, sortField) => {
    if (startsWith(sortField, "-", sortField)) {
      sortField = sortField.slice(1);
      return { ...sortProps, [sortField]: -1 };
    }

    return { ...sortProps, [sortField]: 1 };
  }, {});
};
