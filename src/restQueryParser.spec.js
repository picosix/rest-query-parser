const restQueryParser = require("./restQueryParser");
const parseLimit = require("./parseLimit");
const parseSkip = require("./parseSkip");
const parseSort = require("./parseSort");
const parseRangedFields = require("./parseRangedFields");

jest.mock("./parseLimit");
jest.mock("./parseSkip");
jest.mock("./parseSort");
jest.mock("./parseRangedFields");

const apiConfig = {
  maxLimit: 100,
  defaultLimit: 20,
  maxPage: 10
};
const fakeApp = {
  get: jest.fn(() => apiConfig)
};
const next = jest.fn();

describe("restQueryParser", () => {
  beforeEach(() => {
    parseLimit.mockClear();
    parseSkip.mockClear();
    parseSort.mockClear();
    parseRangedFields.mockClear();

    next.mockClear();
  });

  it("should move to next route if request method is not GET", () => {
    let req = { method: "POST" };
    restQueryParser(apiConfig)(req, {}, next);

    expect(next).toHaveBeenCalledTimes(1);

    expect(parseLimit).not.toBeCalled();
    expect(parseSkip).not.toBeCalled();
    expect(parseSort).not.toBeCalled();
    expect(parseRangedFields).not.toBeCalled();
  });

  it("should calculate paging properties then move to next route", () => {
    let query = { _limit: 15, _page: 2, _sort: "-createdAt" };
    let req = { app: fakeApp, method: "GET", query };
    parseLimit.mockReturnValueOnce(query._limit);

    restQueryParser(apiConfig)(req, {}, next);

    expect(parseLimit).toHaveBeenCalledTimes(1);
    expect(parseLimit).toHaveBeenCalledWith({
      maxLimit: apiConfig.maxLimit,
      defaultLimit: apiConfig.defaultLimit,
      limit: query._limit
    });

    expect(parseSkip).toHaveBeenCalledTimes(1);
    expect(parseSkip).toHaveBeenCalledWith({
      maxPage: apiConfig.maxPage,
      page: query._page,
      limit: query._limit
    });

    expect(parseSort).toHaveBeenCalledTimes(1);
    expect(parseSort).toHaveBeenCalledWith(query._sort);

    expect(parseRangedFields).toHaveBeenCalledTimes(1);
    expect(parseRangedFields).toHaveBeenCalledWith(query);

    expect(next).toHaveBeenCalledTimes(1);
  });
});
