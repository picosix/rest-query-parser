const parseSort = require("./parseSort");

describe("parseSort", () => {
  it("should return empty object if sort param is not a valid string", () => {
    expect(parseSort()).toEqual({});
    expect(parseSort("")).toEqual({});
  });

  it("should return sort object with rule [- => DESC, + => ASC]", () => {
    expect(parseSort("-name,updatedAt")).toEqual({ name: -1, updatedAt: 1 });
  });

  it("should return empty object because of invalid sort", () => {
    expect(parseSort("-, -, -")).toEqual({});
  });
});
