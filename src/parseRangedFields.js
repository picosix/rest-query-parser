const reduce = require("lodash/reduce");
const isObject = require("lodash/isObject");

module.exports = query => {
  const rangeFields = reduce(
    { ...query },
    (parsedFields, fieldValue, fieldName) => {
      if (!isObject(fieldValue) || (!fieldValue.from && !fieldValue.to)) {
        return { ...parsedFields, [fieldName]: fieldValue };
      }

      let rangeCompareValue = {};
      if (fieldValue.from) rangeCompareValue.$gte = fieldValue.from;
      if (fieldValue.to) rangeCompareValue.$lte = fieldValue.to;
      return { ...parsedFields, [fieldName]: rangeCompareValue };
    },
    {}
  );

  return { ...query, ...rangeFields };
};
