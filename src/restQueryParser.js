const parseLimit = require("./parseLimit");
const parseSkip = require("./parseSkip");
const parseSort = require("./parseSort");
const parseRangedFields = require("./parseRangedFields");

const DEFAULT_REST_QUERY_CONFIG = {
  maxLimit: Number(process.env.DEFAULT_REST_QUERY_MAX_LIMIT) || 100,
  defaultLimit: Number(process.env.DEFAULT_REST_QUERY_DEFAULT_LIMIT) || 20,
  maxPage: Number(process.env.DEFAULT_REST_QUERY_MAX_PAGE) || 10
};

const restQueryParser = opts => (req, res, next) => {
  if (req.method !== "GET") return next();

  let { maxLimit, defaultLimit, maxPage } = {
    ...DEFAULT_REST_QUERY_CONFIG,
    ...opts
  };
  let { _limit: limit, _page: page, _sort: sort } = req.query;

  limit = parseLimit({ maxLimit, defaultLimit, limit });
  let skip = parseSkip({ maxPage, page, limit });
  sort = parseSort(sort);

  req._restQueryParsed = { limit, skip, sort };
  req.query = parseRangedFields(req.query);
  return next();
};

module.exports = restQueryParser;
module.exports.DEFAULT_REST_QUERY_CONFIG = DEFAULT_REST_QUERY_CONFIG;
