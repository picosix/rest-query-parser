const parseRangedFields = require("./parseRangedFields");

describe("parseRangedFields", () => {
  let query = {
    _limit: "20",
    _page: "1",
    _sort: "-createdAt",
    accessToken:
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
    createdAt: { from: "2019-01-25T08:39:00.121Z" },
    updatedAt: { to: "2019-01-31T01:12:21.007Z" }
  };

  it("should return converted range filters", () => {
    expect(parseRangedFields(query)).toEqual({
      _limit: "20",
      _page: "1",
      _sort: "-createdAt",
      accessToken:
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
      createdAt: { $gte: "2019-01-25T08:39:00.121Z" },
      updatedAt: { $lte: "2019-01-31T01:12:21.007Z" }
    });
  });
});
