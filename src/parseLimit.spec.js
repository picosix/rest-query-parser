const parseLimit = require("./parseLimit");

const maxLimit = 100;
const defaultLimit = 20;

describe("parseLimit", () => {
  it("should return default limit if request limit is not a valid positive integer", () => {
    expect(parseLimit({ maxLimit, defaultLimit, limit: "#!@^@&!" })).toBe(
      defaultLimit
    );
    expect(parseLimit({ maxLimit, defaultLimit, limit: 0 })).toBe(defaultLimit);
    expect(parseLimit({ maxLimit, defaultLimit, limit: -1 })).toBe(
      defaultLimit
    );
  });

  it("should return max limit if request limit is greater than it", () => {
    expect(
      parseLimit({
        maxLimit,
        defaultLimit,
        limit: maxLimit + 1
      })
    ).toBe(maxLimit);
  });

  it("should return request limit for other case", () => {
    const limit = 15;
    expect(
      parseLimit({
        maxLimit,
        defaultLimit,
        limit
      })
    ).toBe(limit);
  });
});
