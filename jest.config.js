module.exports = {
  verbose: process.env.NODE_ENV === "test",
  setupFilesAfterEnv: ["<rootDir>/jest.setup.js"],
  testEnvironment: "node",
  testMatch: ["**/?(*.)+(spec|test).js"]
};
