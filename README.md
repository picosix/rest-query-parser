# @picosix/rest-query-parser

> Parse your rest query parameters by your way

## Quick Started

### ASAP

```js
...

const restQueryParser = require("@picosix/rest-query-parser");

...

// Use environment variables
server.use(restQueryParser());

...
```

### With your configurations

```js
...

const restQueryParser = require("@picosix/rest-query-parser");

...

// Default options
const restQueryParserOpts = {
  maxLimit: 50,
  defaultLimit:50,
  maxPage: 10
};
server.use(restQueryParser(restQueryParserOpts));

...
```

## How to use

### Setup

```js
...

const express = require("express")
const restQueryParser = require("@picosix/rest-query-parser");

...

const server = express()
server.use(restQueryParser());

server.get("/users", (req, res) => {
  return res.json({
    _restQueryParsed: req._restQueryParsed,
    query: req.query,
  })
})

server.listen(9001)

...
```

### Test

```shell
$ curl http://localhost:9001/users?_limit=10&_page=1&sort=-updatedAt&createdAt[from]=2019-02-10T17:00:00.000Z&createdAt[to]=2019-02-11T16:59:59.999Z
{"_restQueryParsed":{"limit":10,"skip":0,"sort":{"updatedAt":-1}},"query":{"createdAt":{"$gte":"2019-02-10T17:00:00.000Z","$lte":"2019-02-11T16:59:59.999Z"}}}
```

## Environment variables

- `process.env.DEFAULT_REST_QUERY_MAX_LIMIT` (default is `100`): Restrict retuned records on one query. You don't want someone dump your database with one request with query like this `?_limit=999999`.

- `process.env.DEFAULT_REST_QUERY_DEFAULT_LIMIT` (default is `20`): Set default limit if `_limit` is missing.

- `process.env.DEFAULT_REST_QUERY_MAX_PAGE` (default is `10`): Restrict total retuned records. If you defined `DEFAULT_REST_QUERY_MAX_LIMIT` to be `100` and `DEFAULT_REST_QUERY_MAX_PAGE` to be `10`. Not matter how user requested, the total returned records will be `1000 (100*10)`.
