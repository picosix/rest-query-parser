const baseConfig = require("./jest.config");

module.exports = {
  ...baseConfig,
  testMatch: ["**/?(*.)+(spec).js"],
  collectCoverage: true
};
