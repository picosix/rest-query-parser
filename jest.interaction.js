const baseConfig = require("./jest.config");

module.exports = {
  ...baseConfig,
  testMatch: ["**/?(*.)+(test).js"],
  forceExit: true
};
