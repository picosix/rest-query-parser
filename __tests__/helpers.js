const express = require("express");

const restQueryParser = require("../src/restQueryParser");

module.exports.createTestServer = () => {
  const testServer = express();
  testServer.use(restQueryParser());
  return testServer;
};
