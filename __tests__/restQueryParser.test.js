const supertest = require("supertest");

const { createTestServer } = require("./helpers");

describe("restQueryParser", () => {
  describe("_limit", () => {
    let testServer = createTestServer();
    let request;
    const limit = 5;

    beforeAll(() => {
      testServer.get("/", (req, res) => res.json(req._restQueryParsed));
      request = supertest(testServer);
    });

    it("should parse parameter _limit to number", async () => {
      const { body } = await request.get(`/?_limit=${limit}`);

      expect(body.limit).toBe(limit);
    });

    it("should use default value if _limit is missing", async () => {
      const { body } = await request.get("/");

      expect(Number.isInteger(body.limit) && body.limit).toBeTruthy();
    });
  });

  describe("_page", () => {
    let testServer = createTestServer();
    let request;
    const page = 5;

    beforeAll(() => {
      testServer.get("/", (req, res) => res.json(req._restQueryParsed));
      request = supertest(testServer);
    });

    it("should parse parameter _page to number", async () => {
      const { body } = await request.get(`/?_page=${page}`);

      expect(body.skip).toBe((page - 1) * body.limit);
    });

    it("should use default value if _skip is missing", async () => {
      const { body } = await request.get("/");

      expect(body.skip).toBe(0);
    });
  });

  describe("_sort", () => {
    let testServer = createTestServer();
    let request;
    const _sort = "-updatedAt,name";

    beforeAll(() => {
      testServer.get("/", (req, res) => res.json(req._restQueryParsed));
      request = supertest(testServer);
    });

    it("should parse parameter _sort to number", async () => {
      const { body } = await request.get(`/?_sort=${_sort}`);

      expect(body.sort).toEqual({ updatedAt: -1, name: 1 });
    });

    it("should use default value if _skip is missing", async () => {
      const { body } = await request.get("/");

      expect(body.sort).toEqual({});
    });
  });

  describe("Filter by ranged fields", () => {
    let testServer = createTestServer();
    let request;
    const field = "createdAt";
    const fromDate = new Date().toISOString();
    const toDate = new Date().toISOString();
    const query = `${field}[from]=${fromDate}&${field}[to]=${toDate}`;

    beforeAll(() => {
      testServer.get("/", (req, res) => {
        return res.json(req.query);
      });
      request = supertest(testServer);
    });

    it("should parse fields with range filter", async () => {
      const { body } = await request.get(`/?${query}`);
      expect(body[field]).toEqual({
        $gte: fromDate,
        $lte: toDate
      });
    });
  });
});
